// start ex1
const KHU_VUC_A = "kvA";
const KHU_VUC_B = "kvB";
const KHU_VUC_C = "kvC";

const DOI_TUONG_1 = "dt1";
const DOI_TUONG_2 = "dt2";
const DOI_TUONG_3 = "dt3";


function tuyensinh(){
    console.log("yes");

    var kvSelect = document.getElementById("txt-khu-vuc");
    var kvValue = kvSelect.options[kvSelect.selectedIndex].value;
    var dtSelect = document.getElementById("txt-doi-tuong");
    var dtValue = dtSelect.options[dtSelect.selectedIndex].value;

    var diemchuan = document.getElementById("txt-diem-chuan").value*1;

    var diemmon1 = document.getElementById("txt-diem-mon1").value*1;
    var diemmon2 = document.getElementById("txt-diem-mon2").value*1;
    var diemmon3 = document.getElementById("txt-diem-mon3").value*1;

    var result1;
    var result2;

    console.log({diemchuan,kvValue,dtValue,diemmon1,diemmon2,diemmon3});

    if (diemmon1 == 0 || diemmon2 == 0 || diemmon3 == 0){
        console.log("Bạn đã bị trượt");
    }

    var diemKhuVuc = chonKhuVuc(kvValue);
    var diemDoiTuong = chonDoiTuong(dtValue);
    var diemTongKet;

    if (diemDoiTuong == 0 && diemKhuVuc == 0) {
        diemTongKet = diemTongKetKoUuTien(diemmon1,diemmon2,diemmon3);
    } else {
        diemTongKet = diemTongKetCoUuTien(diemmon1,diemmon2,diemmon3,diemKhuVuc,diemDoiTuong);
    }
    console.log({diemTongKet});

    if (diemTongKet >= diemchuan) {
        console.log("Bạn Đã Đậu");
        result2 = "Đậu";
    } else {
        console.log("Bạn Đã Rớt");
        result2 = "Rớt";
    }

    result1 = "Điểm của bạn: " + diemTongKet + ", " + "Bạn Đã " + result2;
    document.getElementById("showResult1").innerHTML = result1;
}

function chonKhuVuc(khuvuc) {
    if (khuvuc == KHU_VUC_A) {
        return 2;
    } else if (khuvuc == KHU_VUC_B) {
        return 1;
    } else if (khuvuc == KHU_VUC_C){
        return 0.5;
    } else {
        return 0;
    }
}

function chonDoiTuong(doituong){
    if (doituong == DOI_TUONG_1) {
        return 2.5;
    } else if (doituong == DOI_TUONG_2) {
        return 1.5;
    } else if (doituong == DOI_TUONG_3) {
        return 1;
    } else {
        return 0;
    }
}

function diemTongKetCoUuTien(mon1,mon2,mon3,kv,dt) {
    var TongketCoUuTien = mon1 + mon2 + mon3 + kv + dt;
    return TongketCoUuTien;
}

function diemTongKetKoUuTien(mon1,mon2,mon3) {
    var TongketKoUuTien = mon1 + mon2 + mon3;
    return TongketKoUuTien;
}

// end ex1

// start ex 2 
function calKw() {
    var nameEl = document.getElementById("txt-ten").value;
    var kwValue = document.getElementById("txt-kw").value*1;
    var result;
    var showResult;

    var gia50kwdau = cal50kwdau(kwValue);
    var gia50kwke = cal50den100kw(kwValue);
    var gia100kwke = cal100den200kw(kwValue);
    var gia150kwke = cal200den350kw(kwValue);
    var giatren350kw = cal350trolen(kwValue);

    if (kwValue <= 50) {
        result = gia50kwdau;
    } else if (kwValue > 50 && kwValue <= 100) {
        result = gia50kwke;    
    } else if (kwValue > 100 && kwValue <= 200) {
        result = gia100kwke;
    } else if (kwValue > 200 && kwValue <= 350) {
        result = gia150kwke;
    } else {
        result = giatren350kw;
    }

    console.log({result});
    showResult = "Họ và Tên: " + nameEl + "; " + "Tiền điện: " + result + "VND";
    document.getElementById("showKw").innerHTML = showResult;
}

function cal50kwdau(sokw) {
    var giatien;
    giatien = sokw*500;
    return giatien;
}

function cal50den100kw(sokw) {
    var giatien;
    giatien = sokw*650;
    return giatien;
}

function cal100den200kw(sokw){
    var giatien;
    giatien = sokw*850;
    return giatien;
}

function cal200den350kw(sokw){
    var giatien;
    giatien = sokw*1100;
    return giatien;
}

function cal350trolen(sokw){
    var giatien;
    giatien = sokw*1300;
    return giatien;
}
// end ex 2